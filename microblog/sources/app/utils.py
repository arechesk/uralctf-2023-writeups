from functools import wraps
from flask import g, request, current_app, redirect, url_for
from app.jwt import decode
from app.models import User
from jwt import encode
import json

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        _jwt = request.cookies.get("Auth")
        if not _jwt:
            return redirect(url_for('login'))
        payload = decode(_jwt, current_app.config['SECRET_KEY'])
        user_id = payload.get('user')
        if not user_id:
            return redirect(url_for('login'))
        user = User.query.filter_by(id=user_id).first()
        if user:
            g.current_user = user
        return func(*args, **kwargs)
    return wrapper

def login_user(user_id: int):
    _jwt = encode({"user": user_id}, current_app.config['SECRET_KEY'], algorithm="HS256")
    return _jwt