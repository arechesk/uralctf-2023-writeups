from app import app
from flask import make_response, render_template, flash, redirect, url_for, request, g
from app.forms import LoginForm, RegistrationForm
from app.models import User, Post
from app import db
from app.utils import login_required, login_user

@app.route('/')
@app.route('/index')
@login_required
def index():
    user = g.current_user
    posts = [
        {
            'author': {'username': 'Jason'},
            'body': 'Когда кажется, что весь мир восстал против тебя, помни, что самолет взлетает против ветра...'
        },
        {
            'author': {'username': 'Jason'},
            'body': 'Гордость парня — это порядочность его девушки.'
        },
        {
            'author': {'username': 'Jason'},
            'body': 'Не слушай тех, кто хвалит. Слушай тех, кто ругает!!!'
        },
        {
            'author': {'username': 'Jason'},
            'body': 'Живи, кайфуй, играй, гуляй, упал - вставай, наглей, ругай, своих роняй, чужих спасай, пельмени, суп, картошка, чай!'
        },
        {
            'author': {'username': 'Jason'},
            'body': 'Если тебе где-то не рады в рваных носках, то и в целых туда идти не стоит'
        },
        {
            'author': {'username': 'Jason'},
            'body': 'На педялях пяточки, еду на ДЕВЯТОЧКЕ'
        },
        {
            'author': {'username': 'user'},
            'body': 'Всем привет! Сегодня готовлю всеми любимое пирожное бизе, хрустящее снаружи и таящее внутри.'
                    'Воздушное, красивое и очень вкусное!'
                    'Берем 2 охлажденных яйца...'
        },
        {
            'author': {'username': 'user'},
            'body': 'Всем еще раз привет, в прошлый раз рецепт дописать не получилось...'
                    'Видимо, в другой раз((('
        },
        {
            'author': {'username': 'user'},
            'body': 'Я в своем познании настолько преисполнился, что я как будто бы уже сто триллионов миллиардов лет проживаю на триллионах и триллионах таких же планет, как эта Земля, мне этот мир абсолютно понятен, и я здесь ищу только одного - покоя, умиротворения и вот этой гармонии, от слияния с бесконечно вечным, от созерцания великого фрактального подобия и от вот этого замечательного всеединства существа, бесконечно вечного, куда ни посмотри, хоть вглубь - бесконечно малое, хоть ввысь - бесконечное большое, понимаешь? А ты мне опять со своим вот этим, иди суетись дальше, это твоё распределение, это твой путь и твой горизонт познания и ощущения твоей природы, он несоизмеримо мелок по сравнению с моим, понимаешь? Я как будто бы уже давно глубокий старец, бессмертный, ну или там уже почти бессмертный, который на этой планете от её самого зарождения, ещё когда только Солнце только-только сформировалось как звезда, и вот это газопылевое облако, вот, после взрыва, Солнца, когда оно вспыхнуло, как звезда, начало формировать вот эти коацерваты, планеты, понимаешь, я на этой Земле уже как будто почти пять миллиардов лет живу и знаю её вдоль и поперёк этот весь мир, а ты мне какие-то... мне не важно на твои тачки, на твои яхты, на твои квартиры, там, на твоё благо. Я был на этой планете бесконечным множеством, и круче Цезаря, и круче Гитлера, и круче всех великих, понимаешь, был, а где-то был конченым говном, ещё хуже, чем здесь. Я множество этих состояний чувствую. Где-то я был больше подобен растению, где-то я больше был подобен птице, там, червю, где-то был просто сгусток камня, это всё есть душа, понимаешь? Она имеет грани подобия совершенно многообразные, бесконечное множество. Но тебе этого не понять, поэтому ты езжай себе , мы в этом мире как бы живем разными ощущениями и разными стремлениями, соответственно, разное наше и место, разное и наше распределение. Тебе я желаю все самые крутые тачки чтоб были у тебя, и все самые лучше самки, если мало идей, обращайся ко мне, я тебе на каждую твою идею предложу сотню триллионов, как всё делать. Ну а я всё, я иду как глубокий старец,узревший вечное, прикоснувшийся к Божественному, сам стал богоподобен и устремлен в это бесконечное, и который в умиротворении, покое, гармонии, благодати, в этом сокровенном блаженстве пребывает, вовлеченный во всё и во вся, понимаешь, вот и всё, в этом наша разница. Так что я иду любоваться мирозданием, а ты идёшь преисполняться в ГРАНЯХ каких-то, вот и вся разница, понимаешь, ты не зришь это вечное бесконечное, оно тебе не нужно. Ну зато ты, так сказать, более активен, как вот этот дятел долбящий, или муравей, который очень активен в своей стезе, поэтому давай, наши пути здесь, конечно, имеют грани подобия, потому что всё едино, но я-то тебя прекрасно понимаю, а вот ты меня - вряд ли, потому что я как бы тебя в себе содержу, всю твою природу, она составляет одну маленькую там песчиночку, от того что есть во мне, вот и всё, поэтому давай, ступай, езжай, а я пошел наслаждаться прекрасным осенним закатом на берегу теплой южной реки. Всё, ступай, и я пойду.'
        },
        {
            'author': {'username': 'administrator'},
            'body': 'UralCTF{it_is_a_fake_flag_you_are_on_a_right_track}'
        },
        {
            'author': {'username': 'secretUser'},
            'body': 'Кажется, нас рассекретили...'
        },
        {
            'author': {'username': 'secretUser'},
            'body': 'UralCTF{4l60r17hm_n0n3_4774ck}'
        }
    ]
    return render_template('index.html', title='Home',  user=user, posts=posts)

'''
user:user
Jason:JSON
administrator:qN25IGw2OnSVutt
secretUser:k6ifoNdHSrcyEBH
'''
@app.route('/login', methods=['GET', 'POST'])
def login():
    # if current_user.is_authenticated:
    #     return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        response = make_response(redirect(url_for('index')))
        response.set_cookie("Auth", login_user(user.id))
        return response
    return render_template('login.html', title='Sign in', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    # if current_user.is_authenticated:
    #     return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app.route("/logout", methods=['GET'])
def logout():
    r = make_response(redirect(url_for('index')))
    r.delete_cookie("Auth")
    return r