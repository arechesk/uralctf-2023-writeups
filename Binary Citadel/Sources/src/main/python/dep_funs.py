def read_file(file_path: str) -> str:
    try:
        with open(file_path, 'r', encoding="utf-8") as f:
            return f.read()
    except Exception as e:
        return str(e)
