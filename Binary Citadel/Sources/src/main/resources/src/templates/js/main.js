const url = window.location.href;


async function postToServer(bodyjson){
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(bodyjson)
    }).then(response => response.json())

    return JSON.stringify(response);
}

async function getFromServer(path){
    const response = await fetch(url + path).then(response => response.json());
    return JSON.stringify(response);
}


async function activate(object_type){
    let responseStatus = null;
    if (object_type == "68 75 6D 61 6E"){
        responseStatus = await getFromServer("7f5cb74af5d7f4b82200738fdbdc5a45");
        alert("HUMAN DETECTED THUS INCIDENT HAS BEEN REPORTED\n" + responseStatus.toString())
        console.log("ALERT");
    }
    else if (object_type == "52 4F 42 4F 54"){
        var bodyjson = robotMenu();
        responseStatus = await postToServer(bodyjson);
    }
    console.log(responseStatus);
}


function robotMenu(){
    var setUrl = document.getElementById("send").value;

    return {
        'url': setUrl
    };
}
