# Binary Citadel

**Условие задания:**
> Мы нашли странный сайт, но никак не можем попасть внутрь, похоже, он предназначался только для роботов.
Интересно, что они замышляют? Помогите нам это выяснить.

**Решение:**
Прежде всего нужно перевести все из hex, воспользуемся "cyberchef" fromhex, сказано ввести любимый сайт, посмотрим, что есть в robots.txt, найдем подсказку: "We found out that humans like webhook sites"
Нужно в поле ввести ссылку на какой-либо вебхук тест сайт.

Но нам не дает активировать кнопку, откроем devtools и найдем нужную нам функцию.
```

async function activate(object_type){
    let responseStatus = null;
    if (object_type == "68 75 6D 61 6E"){
        responseStatus = await getFromServer("7f5cb74af5d7f4b82200738fdbdc5a45");
        alert("HUMAN DETECTED THUS INCIDENT HAS BEEN REPORTED\n" + responseStatus.toString())
        console.log("ALERT");
    }
    else if (object_type == "52 4F 42 4F 54"){
        var bodyjson = robotMenu();
        responseStatus = await postToServer(bodyjson);
    }
    console.log(responseStatus);
}

```

Запустим её через консоль с переданным аргументом "52 4F 42 4F 54",
а в поле ввода ссылка на тест вебхук сайт.
```
activate("52 4F 42 4F 54")
```

Увидим в заголовках кастомный header:
```x-custom-header: "4D 41 43 48 49 4E 45"```

Обратимся к сайту с этим заголовком с помощью утилиты "curl"

```
curl -i -G http://0.0.0.0:5556 -H "X-Custom-Header: 4D 41 43 48 49 4E 45"
```

Получим ответом другой шаблон, переведем сообщения из hex, получим флаг: `UralCTF{17_4lw4y5_h45_b33n_ju57_4_pl4y}`