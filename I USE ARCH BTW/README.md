# I USE ARCH BTW

**Условие задания:**
> О чем он хочет нам сказать?

**Решение:**
Первым делом нужно загуглить I use arch btw, найдем, что это эзотерический язык програмирования: `https://github.com/overmighty/i-use-arch-btw` следуем гайду установки и после запускаем `i-use-arch-btw i-use-arch-btw.archbtw`
Получим флаг: `UralCTF{1_u53_4rch_l1nux_h0w_d1d_y0u_kn0w}`