# Goofy task

**Описание**
Мы уволили одного нашего програмиста и обнаружили что перед уходом с его рабочей станции шёл странный трафик, глянь что там.


**Решение**
- Открываем *.pcap файл вайршарком, сразу же видим FTP трафик
- Вильтруем по ftp-data
- Следуем по TCP пакетам и сохраняем полученное в raw формате, как *.png
- Читаем флаг UralCTF{th3_pC4p_tr4V3ler}