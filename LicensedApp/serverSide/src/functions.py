from Crypto.PublicKey import RSA


def verify_pub_key(pub_key_to_verify: str, priv_key: str) -> bool:

    private_key = RSA.importKey(priv_key)
    public_key = private_key.publickey().exportKey().decode("utf-8").replace("\n", "")

    if public_key == pub_key_to_verify:
        return True
    else:
        return False
