# I USE ARCH BTW

**Условие задания:**
> О чем он хочет нам сказать?

**Решение:**
Для реверса апк воспользуемся утилитой "jadx"
```https://github.com/skylot/jadx```

```jadx-gui LisencedApp.apk```

Зайдем в MainActivity, видим, что происходит проверка, и выпоняется функция обращения к api для получения флага, можно решить патчем или самим сделать запрос к api
Сами сделаем запрос:
Из MainActivity смотрим откуда берется ключ -> ConfigDataClass -> R.row.key -> 
```
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDasI4Wej7DAr+fyiaJAFIazoOB
iyUuA0ipoJeajC82ntgLt8CjKhOH8c7INk7kFORYyz+7N1x8TuAF4puS4k0y9+yu
pW1d1EZmFozkylqUcUJFMs0F1gTGmDB4//x4C4WnTxUtwCJZjz5LlSCoSMRYmnoe
CGPIDeZsDWOnjo22aQIDAQAB
-----END PUBLIC KEY-----
```
Аналогично порт и адрес сервера
```
address = 5.1.53.41
port = 5555
```

Посмотрим теперь, как составляется запрос:
`ApiWorker.getFile(String key, String req)`
видим, что отправляется json:
`{"key": key.replace("\n")}`
У ключа заменяются переходы на новую строку.
Воспользуемся утилитой "curl"
Составим запрос:
```
curl -i -X POST http://5.1.43.41:5555/file -H "Content-Type: application/json" -d '{"key": "-----BEGIN PUBLIC KEY-----MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDasI4Wej7DAr+fyiaJAFIazoOBiyUuA0ipoJeajC82ntgLt8CjKhOH8c7INk7kFORYyz+7N1x8TuAF4puS4k0y9+yupW1d1EZmFozkylqUcUJFMs0F1gTGmDB4//x4C4WnTxUtwCJZjz5LlSCoSMRYmnoeCGPIDeZsDWOnjo22aQIDAQAB-----END PUBLIC KEY-----"}'
```
Получим ответом флаг: `flag = UralCTF{d0n7_3v3n_n33d_4_l1c3n53}`