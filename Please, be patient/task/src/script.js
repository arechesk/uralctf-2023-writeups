// Don't forget to obfuscate the code with https://obfuscator.io/#code and save the result
// as `script.min.js` in the same directory.

const msecs = 1337137*1000;
const targetRun = Date.now() + msecs;
function upd() {
    let sec = document.getElementById('sec');
    let currentRun = Date.now();
    let diff = Math.round((targetRun - currentRun) / 1000);
    sec.innerHTML = diff.toString();
    if (diff > 0) {
        if (diff === 1) {
            document.getElementById('pl').innerHTML = '';
        }
        setTimeout(upd, 1000);
    } else {
        document.getElementById('content').innerHTML = 'Yeah, the time is out. Your' +
            ' flag is <br><code class="hidden">UralCTF{Ar3Y0uAT1meTrAv3ll3r?}</code>'
    }
}
document.getElementById('noscript').remove();
upd();
